This app is a node server to access the Twitter tending topics API and streaming API.

This is intended to be the backend for an `https://gitlab.com/sharmanikita/trending-topics-in-dubai---frontend`.

Installation
Clone the repository
npm install
Getting a Twitter access token
Get a Twitter access token. Access./config.js and add your key information.

Start the server
* git clone
* npm install
* node index

